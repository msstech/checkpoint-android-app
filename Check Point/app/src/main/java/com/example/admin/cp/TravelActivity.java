package com.example.admin.cp;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Admin on 01-04-2015.
 */
public class TravelActivity extends Activity {
    EditText e1,e2;
    EditText e3,e4,e5,e6,e7,e8,e9;
    SQLiteDatabase db=null;
    int id;
    ListView lv1;
    String pn,m,m1,r,t;
    private NotificationManager mNotificationManager;
    private int notificationID = 100;
    private int numMessages = 1;
    NotificationCompat.Builder  mBuilder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.travel_main);
        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);


        e5=(EditText)findViewById(R.id.checkSearch);
        e6=(EditText)findViewById(R.id.Search);
        e7=(EditText)findViewById(R.id.makeSearch);
        e8=(EditText)findViewById(R.id.modelSearch);
        e9=(EditText)findViewById(R.id.plateSearch);
        e7.setText(constants.pn);
        e8.setText(constants.ma);
        e9.setText(constants.mo);
        Cursor c3;
        c3 = db.rawQuery("select * from CheckPoint", null);

        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e5.setText(pn);
                // a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e6.setText(strDate);
        e5.setClickable(false);
        e5.setFocusable(false);
        e6.setClickable(false);
        e6.setFocusable(false);
        e7.setClickable(false);
        e7.setFocusable(false);
        e8.setClickable(false);
        e8.setFocusable(false);
        e9.setClickable(false);
        e9.setFocusable(false);

        lv1 = (ListView) findViewById(R.id.country_list);
        DisplayAdapter1 disadpt = new DisplayAdapter1(TravelActivity.this,constants.an,constants.vn);
        // lv1.setAdapter(new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1, titledb));
        lv1.setAdapter(disadpt);
        mBuilder =
                new NotificationCompat.Builder(this);


        mBuilder.setContentTitle("Trip Verification");

        mBuilder.setContentText("Trip"+"  Verified");
        mBuilder.setTicker("Status Alert!");
        mBuilder.setSmallIcon(R.drawable.noti);

      /* Increase notification number every time a new notification arrives */
        mBuilder.setNumber(++numMessages);



        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);







        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
       // getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("TRIP DETAIL");
        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);

        e1 = (EditText) findViewById(R.id.editText2);
        e2 = (EditText) findViewById(R.id.editText3);

        db.execSQL("create table if not exists Trip1 (ID INTEGER PRIMARY KEY AUTOINCREMENT,source VARCHAR ,Destination VARCHAR   )");



    }
    public void save (View v) {
        constants.cpkd="yes";


         if((e1.getText().toString().equals(""))&&(e2.getText().toString().equals("")))
        {
            show("enter data");

        }
       else if((e1.getText().toString().equals(""))||(e2.getText().toString().equals("")))
        {
            show("enter data in both fields");

        }
        else {
             Cursor c = db.rawQuery("select * from RTA ", null);

             if (c.getCount() > 0) {
                 c.moveToFirst();
                 do {
                     id = c.getInt(c.getColumnIndex("ID"));
                     pn = c.getString(1);
                     m = c.getString(2);
                     m1 = c.getString(3);
                     t = c.getString(4);
             r = c.getString(5);

         } while (c.moveToNext());

    }






    constants.cid= constants.cid++;

             System.out.println("prevnjn"+constants.pn);

             db.execSQL("update Checkpoint_Trip set CHECK_POINT_TRIP_ID='" + "xyz0001"+constants.cid + "',RTA_VERIFICATION='" + constants.rtav+ "',RTA_REFERENCE_NUMBER='" +r+ "',SOURCE='" + e1.getText().toString() + "',DESTINATION='" + e2.getText().toString() + "',CAR_PARKED='" + constants.cpkd + "' where PLATE_NUMBER = '" + constants.pn + "'");
             db.execSQL("insert into Trip1 (source , Destination ) values('" + e1.getText().toString() + "','" + e2.getText().toString() + "')");

          //   mNotificationManager.notify(Integer.parseInt("1000"), mBuilder.build());

             Intent i2 = new Intent(TravelActivity.this, Preview1.class);
             startActivity(i2);
             show("data saved");


         }

    }
    public void cancel (View v) {
    finish();
    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

}



