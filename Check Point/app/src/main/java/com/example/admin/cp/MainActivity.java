package com.example.admin.cp;

import android.app.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class MainActivity extends Activity {
    EditText e1,e2,e3,e4,e5,e6;
    String pid,fname,dob,ad,g,dl,s,rn,cpid;
    String n,p;
    ArrayList<String> a= new ArrayList();
    ArrayList<String> b= new ArrayList();
    ArrayList<String> c1= new ArrayList();
    ArrayList<String> d= new ArrayList();
    ArrayList<String> e= new ArrayList();
    ArrayList<String> f= new ArrayList();
    ArrayList<String> j= new ArrayList();
    SharedPreferences mPrefs;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
TextView t;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
String status;

    SQLiteDatabase db=null;
    int id;
    ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        t=(TextView)findViewById(R.id.tv);
        e2=(EditText)findViewById(R.id.checkSearch);
        e3=(EditText)findViewById(R.id.Search);
        e4=(EditText)findViewById(R.id.makeSearch);
        e5=(EditText)findViewById(R.id.modelSearch);
        e6=(EditText)findViewById(R.id.plateSearch);
        e6.setText(constants.pn);
        e4.setText(constants.ma);
        e5.setText(constants.mo);
        Cursor c3;
        if(constants.p.equals("true"))
        {
            t.setVisibility(View.INVISIBLE);
        }
        else
        {
            t.setVisibility(View.VISIBLE);
        }
        c3 = db.rawQuery("select * from CheckPoint", null);

        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e2.setText(pn);
                // a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e3.setText(strDate);
        e2.setClickable(false);
        e2.setFocusable(false);
        e3.setClickable(false);
        e3.setFocusable(false);
        e4.setClickable(false);
        e4.setFocusable(false);
        e5.setClickable(false);
        e5.setFocusable(false);
        e6.setClickable(false);
        e6.setFocusable(false);

        lv1 = (ListView) findViewById(R.id.country_list);
        DisplayAdapter1 disadpt = new DisplayAdapter1(MainActivity.this,constants.an,constants.vn);
        // lv1.setAdapter(new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1, titledb));
        lv1.setAdapter(disadpt);

        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayHomeAsUpEnabled(true);
       // getActionBar().setHomeButtonEnabled(true);
        //getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("PERSON SEARCH");

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean welcomeScreenShown = mPrefs.getBoolean("k", false);
        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);

        e1 = (EditText) findViewById(R.id.textView);

      //  pind=e1.getText().toString();e1 = (EditText) findViewById(R.id.editText);
        db.execSQL("create table if not exists PNID (ID INTEGER PRIMARY KEY AUTOINCREMENT,PID VARCHAR ,DriverLicense VARCHAR , Fname VARCHAR , DOB VARCHAR , Ad VARCHAR , GENDER VARCHAR   )");
        if (!welcomeScreenShown) {
            db.execSQL("insert into PNID (PID , DriverLicense , Fname , DOB , Ad , GENDER ) values('" + "pn0005" + "','" + "dl0005" + "','" + "jai" + "','" + "20/2/1993" + "','" + "CHENNAI" + "','" + "MALE" + "')");
            db.execSQL("insert into PNID (PID , DriverLicense , Fname , DOB , Ad , GENDER ) values('" + "pn0010" + "','" + "dl0010" + "','" + "manoj" + "','" + "25/2/1993" + "','" + "SALEM" + "','" + "MALE" + "')");
            db.execSQL("insert into PNID (PID , DriverLicense , Fname , DOB , Ad , GENDER ) values('" + "pn0001" + "','" + "dl0001" + "','" + "ANIDHA" + "','" + "22/2/1995" + "','" + "PORUR" + "','" + "FEMALE" + "')");
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putBoolean("k", true);
            editor.commit();
        }
    }

    public void click (View v) {
        Cursor cc = db.rawQuery("select * from RTA where PLATE_NUMBER='" + constants.pn + "'", null);
        Cursor cc1 = db.rawQuery("select * from CheckPoint", null);
        if (cc1.getCount() > 0) {
            cc1.moveToFirst();
            do {
                id = cc1.getInt(cc1.getColumnIndex("ID"));
                cpid = cc1.getString(1);

            } while (cc1.moveToNext());
        }
        if (cc.getCount() > 0) {
            cc.moveToFirst();
            do {
                id = cc.getInt(cc.getColumnIndex("ID"));
                rn = cc.getString(6);

            } while (cc.moveToNext());
        }
        Cursor c12 = db.rawQuery("select * from RTA WHERE DL='" + e1.getText().toString() + "'", null);
        Cursor c = db.rawQuery("select * from person WHERE STATE_ID='" + e1.getText().toString() + "'", null);

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
              id = c.getInt(c.getColumnIndex("ID"));
                pid = c.getString(28);

                dl = c.getString(28);
                fname = c.getString(1);

                dob = c.getString(14);
                ad = c.getString(8);
                g = c.getString(7);
                a.add(pid);
                b.add(dl);
                c1.add(fname);
                d.add(dob);
                e.add(ad);
                f.add(g);
                j.add(String.valueOf(id));
            } while (c.moveToNext());
        //    for (int i = 0; i < c.getCount(); i++) {

               // if (e1.getText().toString().equalsIgnoreCase(a.get(i)))

               // {
                    constants.an.add(c1.get(0));
                    constants.vn.add(a.get(0));
                    constants.an1.add(c1.get(0));
                    constants.vn1.add(a.get(0 ));
                   constants.p="false";
                    Intent intent = new Intent(MainActivity.this, DispalyActivity.class);
                    intent.putExtra("pid", e1.getText().toString());
                    intent.putExtra("name", fname);
                    intent.putExtra("dob", dob);
                    intent.putExtra("ad",ad);
                    intent.putExtra("g", g);
                    intent.putExtra("rn", rn);

                    intent.putExtra("cpid", cpid);
                    // intent.putExtra("pn", a.get(i));
                    //  intent.putExtra("dl", b.get(i));
                    //  intent.putExtra("id", j.get(i));
                    startActivity(intent);
                    show("PNID verified");
                    // show(s);
                    status = "true";
                   /* break;
                } else if (e1.getText().toString().equalsIgnoreCase(b.get(i))) {
                    System.out.println("2");

                    Intent intent = new Intent(MainActivity.this, DispalyActivity.class);
                    intent.putExtra("pid", e1.getText().toString());
                    intent.putExtra("name", c1.get(i));
                    intent.putExtra("dob", d.get(i));
                    intent.putExtra("ad", e.get(i));
                    intent.putExtra("g", f.get(i));
                    intent.putExtra("rn", rn);
                    constants.p="false";
                    intent.putExtra("cpid", cpid);
                    constants.an.add(c1.get(i));
                    constants.vn.add(a.get(i));
                    constants.an1.add(c1.get(i));
                    constants.vn1.add(a.get(i));
                    // intent.putExtra("pn", a.get(i));
                    // intent.putExtra("dl", b.get(i));
                    // intent.putExtra("id", j.get(i));
                    startActivity(intent);
                    show("License verified");
                    //show(s);
                    status = "true";
                    break;
                } else if (e1.getText().toString().equals("")) {
                    System.out.println("3");
                    show("enter details");*/

                } else {
                    // status="false";
                    t.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MainActivity.this, CreatePassengerActivity.class);
                    intent.putExtra("pid", e1.getText().toString());
                    intent.putExtra("rn", rn);
                    constants.p="false";
                    startActivity(intent);
                    show("Create new Details");
                }

    }
    public void add (View v) {
        t.setVisibility(View.VISIBLE);
        Intent intent = new Intent(MainActivity.this, CreatePassengerActivity.class);
        intent.putExtra("pid", e1.getText().toString());
        intent.putExtra("rn", rn);
        constants.p="false";
        startActivity(intent);
        show("Create new Details");
    }
    public void click1 (View v) {

        Intent intent = new Intent(MainActivity.this, TravelActivity.class);
        startActivity(intent);
        constants.p="false";
    }


    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

}
