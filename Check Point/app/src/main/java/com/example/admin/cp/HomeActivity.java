package com.example.admin.cp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

public class HomeActivity extends Activity {
    SQLiteDatabase db=null;
    String u="username";
    String p="password";
    String u1,p1;
    String j;
    EditText usernames,passwords;
    SharedPreferences mPrefs;
    int attempt=0,id;
        Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_activity);



        usernames = (EditText) findViewById(R.id.textView);

        passwords= (EditText) findViewById(R.id.textView3);
        db=openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        b=(Button)findViewById(R.id.button1);
        db.execSQL("create table if not exists CheckPoint (ID INTEGER PRIMARY KEY AUTOINCREMENT,CHECK_POINT VARCHAR , PLACE VARCHAR )");
        db.execSQL("CREATE TABLE if not exists login (ID INTEGER PRIMARY KEY AUTOINCREMENT,USER_NAME VARCHAR,PASSWORD VARCHAR )");
        db.execSQL("create table if not exists Checkpoint_Trip (ID INTEGER PRIMARY KEY AUTOINCREMENT,CHECK_POINT_TRIP_ID VARCHAR,PLATE_NUMBER VARCHAR ,RTA_VERIFICATION VARCHAR,RTA_REFERENCE_NUMBER VARCHAR, MAKE VARCHAR,MODEL VARCHAR,TYPE VARCHAR,SOURCE VARCHAR,DESTINATION VARCHAR,CAR_PARKED VARCHAR )");
        db.execSQL("create table if not exists PersonDetails (ID INTEGER PRIMARY KEY AUTOINCREMENT,CHECK_POINT_CROSS_ID INTEGER , PERSON_ID VARCHAR , PERSON_REF_NO VARCHAR , NAME VARCHAR  , DATE_OF_BIRTH DATETIME, ADDRESS VARCHAR , VERIFICATION_STATUS VARCHAR, IS_DRIVER VARCHAR )");
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean welcomeScreenShown = mPrefs.getBoolean(u, false);

        if (!welcomeScreenShown) {

        db.execSQL("insert into login (USER_NAME , PASSWORD) values('" + "username" + "','" + "password" + "')");
            db.execSQL("insert into CheckPoint (CHECK_POINT , PLACE) values('" + "chk0001" + "','" + "chennai" + "')");
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putBoolean(u, true);
            editor.commit();

            }



       // db.execSQL("create table if not exists login(ID INTEGER PRIMARY KEY AUTOINCREMENT,username varchar,password blob)");
     //   db.execSQL("insert into VehicleDetails (VEHICLE_TRACK_NUMBER,VEHICLE_NUMBER,PASSENGER_COUNT,PASSENGER_ADULT,PASSENGER_CHILD,PASSENGER_INFANT,VEHICLE_TYPE,ROUND_TRIP,SOURCE,DESTINATION,DRIVER_IDENTITY) values('" + .getText().toString() + "','" +passwords.getText().toString()+ "')");


      //  mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

       // try {
            //normalTextEnc = AESHelper.encrypt(seedValue, normalText);
          //  System.out.println("encrypt"+normalTextEnc);
       // }catch (Exception e) {
            // TODO Auto-generated catch block
          //  e.printStackTrace();
       // }
        // second argument is the default to use if the preference can't be found
     //   Boolean welcomeScreenShown = mPrefs.getBoolean(db_insert, false);
        //if (!welcomeScreenShown) {

        //    db.execSQL("insert into login (username,password) values('" + u + "','" +normalTextEnc+ "')");

        //    SharedPreferences.Editor editor = mPrefs.edit();
          //  editor.putBoolean(db_insert, true);
         //   editor.commit();


      //  }
    }
    public void click (View v) {

        String s=usernames.getText().toString();
        String s1=passwords.getText().toString();



        Cursor c=db.rawQuery("select * from login ",null);

        if(c.getCount()>0)
        {
            c.moveToFirst();
            do {
                int id = c.getInt(c.getColumnIndex("ID"));
                u1 = c.getString(1);
                p1 = c.getString(2);
              //  try {
                //   normalTextDec = AESHelper.decrypt(seedValue, p1);
               // } catch (Exception e) {
                  //  e.printStackTrace();
              //  }
               // System.out.println("encryptghh"+p1);
               // System.out.println("encryptghh"+normalTextDec);
            }while(c.moveToNext());

            if(s==null||s==""||s.length()<3)
            {
                show("Please Enter username.");
            }
            else if(s1==null||s1==""||s1.length()<3)
            {
                show("Please Enter password.");
            }

            else if(s.equals(u1)&&s1.equals(p1))
            {
                show(" SUCCESS");
                Intent intent = new Intent(HomeActivity.this, Plate.class);

                startActivity(intent);
            }
            else {
                attempt++;
                show(" INCORRECT AUTH. " + attempt + " Attempt");
                if (attempt > 3) {
                    show(" Login Blocked");
                    b.setVisibility(View.GONE);
                    //dbhelper.close();
                    getApplicationContext().deleteDatabase("mydb1");
                }
            }
            }
        else {
            show(" DB crashed");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
