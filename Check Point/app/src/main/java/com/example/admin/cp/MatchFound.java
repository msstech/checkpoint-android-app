package com.example.admin.cp;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Button;
import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Blufour1 on 01-04-2015.
 */
public class MatchFound extends Activity {
    private NotificationManager mNotificationManager;
    private int notificationID = 100;
    private int numMessages = 0;
    NotificationCompat.Builder  mBuilder;
    EditText makeEdit,modelEdit,typeEdit,e1,e2;
    SQLiteDatabase db=null;
    String s;
    Button verified;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.match_found);
        Bundle bundle = this.getIntent().getExtras();
        s = bundle.getString("pn");

        mBuilder =
                new NotificationCompat.Builder(this);
        e1=(EditText)findViewById(R.id.checkSearch);
        e2=(EditText)findViewById(R.id.Search);

        mBuilder.setContentTitle("Vehicle Verification");

        mBuilder.setContentText(s+"  Verified");
        mBuilder.setTicker("Status Alert!");
        mBuilder.setSmallIcon(R.drawable.noti);

      /* Increase notification number every time a new notification arrives */
        mBuilder.setNumber(++numMessages);



        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("VEHICLE DETAILS");
        makeEdit=(EditText)findViewById(R.id.makeEdit);
        modelEdit=(EditText)findViewById(R.id.modelEdit);
        typeEdit=(EditText)findViewById(R.id.typeEdit);
        verified=(Button)findViewById(R.id.verified);

        db=openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        Cursor
        c3 = db.rawQuery("select * from CheckPoint", null);

        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e1.setText(pn);
                //a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e2.setText(strDate);
        e1.setClickable(false);
        e1.setFocusable(false);
        e2.setClickable(false);
        e2.setFocusable(false);
        System.out.println(s);
        Cursor c = db.rawQuery("select * from Vehicle WHERE  l_p='"+s+"'", null);
       // Cursor c1 = db.rawQuery("select * from RTA WHERE  PLATE_NUMBER='"+s+"'", null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            do {

                String ma = c.getString(4);

                String mo = c.getString(5);
                String ty = c.getString(6);
                constants.ma=ma;
                constants.mo=mo;
                makeEdit.setText(ma);
                modelEdit.setText(mo);
                typeEdit.setText(ty);
                db.execSQL("insert into Checkpoint_Trip (PLATE_NUMBER  , MAKE ,MODEL ,TYPE ) values('" + s + "','" + ma + "','" + mo + "','" + ty + "')");
                modelEdit.setClickable(false);
                modelEdit.setFocusable(false);
                makeEdit.setClickable(false);
                makeEdit.setFocusable(false);
                typeEdit.setClickable(false);
                typeEdit.setFocusable(false);




            } while (c.moveToNext());
        }
      /*  else if (c1.getCount() > 0) {
            c1.moveToFirst();
            do {
                System.out.println("2");

                String ma = c1.getString(2);

                String mo = c1.getString(3);
                String ty = c1.getString(4);
                constants.ma=ma;
                constants.mo=mo;
                makeEdit.setText(ma);
                modelEdit.setText(mo);
                typeEdit.setText(ty);

                modelEdit.setClickable(false);
                modelEdit.setFocusable(false);
                makeEdit.setClickable(false);
                makeEdit.setFocusable(false);
                typeEdit.setClickable(false);
                typeEdit.setFocusable(false);

                db.execSQL("insert into Checkpoint_Trip (PLATE_NUMBER  , MAKE ,MODEL ,TYPE ) values('" + s + "','" + ma + "','" + mo + "','" + ty + "')");



            } while (c1.moveToNext());
        }*/
        else
        {

        }
     verified.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             constants.rtav="yes";
             Intent i=new Intent(MatchFound.this,MainActivity.class);
             startActivity(i);
           //  mNotificationManager.notify(Integer.parseInt("100000"), mBuilder.build());
         }
     });


    }
    public void nv (View v) {
        Intent intent1 = new Intent(MatchFound.this, Plate.class);

        startActivity(intent1);
        constants.rtav="no";
    }
}