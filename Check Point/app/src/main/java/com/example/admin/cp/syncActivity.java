package com.example.admin.cp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;



/**
 * Created by Admin on 27-03-2015.
 */
public class syncActivity extends Activity {
    // Progress Dialog
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;

    // url to get all products list
    private static String url_all_products = "http://192.168.0.20/vehicle/get_all_products.php";
    private static String url_all_products1 = "http://192.168.0.6/vehicle/get_all_products1.php";
    private static String url_all_products2 = "http://192.168.0.20/vehicle/get_all_products3.php";
    private static String url_all_products3 = "http://192.168.0.6/vehicle/get_all_products3.php";
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";

    // products JSONArray
    JSONArray products = null;
    SQLiteDatabase db=null;
    private ProgressDialog pDialog;
    String vn,Sr,Dt,vtr,rt,id1;
    JSONParser jsonParser = new JSONParser();
    int pc,vt,pa,pcd,pi,id,s,opid,cpid;
    String d;

    // url to create new product
    private static String url_create_product = "http://192.168.0.14/vehicle/create_product.php";

    // JSON Node names

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plate_number);





        pDialog = new ProgressDialog(syncActivity.this);
        pDialog.setMessage("Please wait ..");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                      //  new CreateNewProduct().execute();
                     //   new vehicledetail().execute();
                       new persondetail().execute();
                    }
                }
        ).start();

    }


    class CreateNewProduct extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            db= openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
            db.execSQL("create table if not exists VehicleDetails (id INTEGER , customer_id VARCHAR , driver_license_number VARCHAR , control_number VARCHAR , issue_date date , expiry_date date , classification VARCHAR , luby VARCHAR , ludt date , transaction_id VARCHAR , mag_track1 VARCHAR , mag_track2 VARCHAR , mag_track3 VARCHAR , application_status VARCHAR  )");
        }
        protected String doInBackground(String... args) {

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        int id = c.getInt("id");
                        String cid = c.getString("customer_id");
                        String dln = c.getString("driver_license_number");
                        String cn = c.getString("control_number");
                        String idt = c.getString("issue_date");
                        String edt = c.getString("expiry_date");
                        String cfn = c.getString("classification");
                        String lb = c.getString("luby");
                        String ld = c.getString("ludt");
                        String tid = c.getString("transaction_id");
                        String mt1 = c.getString("mag_track1");
                        String mt2 = c.getString("mag_track2");
                        String mt3 = c.getString("mag_track3");
                        String as = c.getString("application_status");

                        db.execSQL("insert into VehicleDetails (id,customer_id,driver_license_number,control_number,issue_date,expiry_date,classification,luby,ludt,transaction_id,mag_track1,mag_track2,mag_track3,application_status) values('" + id + "','" + cid + "','" + dln + "','" + cn + "','" + idt + "','" + edt + "','" + cfn + "','" + lb + "','" + ld + "','" + tid + "','" + mt1 + "','" + mt2 + "','" + mt3 + "','" + as + "')");


                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
           // pDialog.dismiss();
         //   new vehicledetail().execute();
        }

    }
    class persondetail extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            db= openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
            db.execSQL("create table if not exists person (ID INTEGER , FIRST_NAME VARCHAR , SECOND_NAME VARCHAR , THIRD_NAME VARCHAR ,FOURTH_NAME VARCHAR , MARITIAL_STATUS VARCHAR , LIVING_STATUS VARCHAR,GENDER VARCHAR , ADDRESS VARCHAR , CITY VARCHAR,REGION VARCHAR , PHONE_NUMBER VARCHAR , MOBILE_NUMBER VARCHAR , EMAIL VARCHAR ,DATE_OF_BIRTH VARCHAR , PLACE_OF_BIRTH VARCHAR , HEIGHT VARCHAR,WEIGHT VARCHAR , EYE_COLOR VARCHAR , HAIR_COLOR VARCHAR,OCCUPATION VARCHAR , MOTHER_FIRST_NAME VARCHAR , MOTHER_SECOND_NAME VARCHAR,MOTHER_THIRD_NAME VARCHAR , MOTHER_FOURTH_NAME VARCHAR ,  CREATED_DATE VARCHAR , LUDT VARCHAR,LUBY VARCHAR , STATE_ID VARCHAR , FAMILY_HEAD VARCHAR  , POSTCODE VARCHAR ,CITIZEN_STATUS VARCHAR , NATURALIZATION_ID VARCHAR , REGION_ID VARCHAR,CAN_VOTE VARCHAR , STATE_ID_PROCESS_ID VARCHAR , PROCESS_TOKEN_NO VARCHAR,STATE_ID_STATUS VARCHAR  , STATE_ID_PROCESS_STATUS VARCHAR , FAMILY_STATE_ID VARCHAR , PICKUP_LOCATION VARCHAR ,ENROLL_LOCATION VARCHAR , ENROLL_DISTRICT VARCHAR , SERVICE_DISTRICT VARCHAR,TIME_OF_BIRTH VARCHAR , BIRTH_CERTIFICATE_PROCESS_STATUS VARCHAR , AUTH_CERTIFICATE_PROCESS_STATUS VARCHAR,FATHER_FIRST_NAME VARCHAR , FATHER_SECOND_NAME VARCHAR , FATHER_THIRD_NAME VARCHAR,FATHER_FOURTH_NAME VARCHAR   )");




        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products3, "GET", params);
            Runtime.getRuntime().gc();
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        int id = c.getInt("ID");
                        String fn = c.getString("FIRST_NAME");
                        String sn = c.getString("SECOND_NAME");
                        String tn = c.getString("THIRD_NAME");
                        String frn = c.getString("FOURTH_NAME");
                        String ms = c.getString("MARITIAL_STATUS");
                        String ls = c.getString("LIVING_STATUS");
                        String g = c.getString("GENDER");
                        String a = c.getString("ADDRESS");
                      //  System.out.println("asaddfdsdsa"+a);
                        String a1=a.replaceAll("'", "\''");
                       // System.out.println("asasdsa"+a1);
                        String cy = c.getString("CITY");
                        String r = c.getString("REGION");
                        String pn = c.getString("PHONE_NUMBER");
                        String mn = c.getString("MOBILE_NUMBER");
                        String e = c.getString("EMAIL");
                        String dob = c.getString("DATE_OF_BIRTH");
                        String pob = c.getString("PLACE_OF_BIRTH");
                        String h = c.getString("HEIGHT");
                        String wgt = c.getString("WEIGHT");
                        String ec = c.getString("EYE_COLOR");
                        String hc = c.getString("HAIR_COLOR");
                        String o = c.getString("OCCUPATION");
                        String mfn = c.getString("MOTHER_FIRST_NAME");
                        String msn = c.getString("MOTHER_SECOND_NAME");
                        String mtn = c.getString("MOTHER_THIRD_NAME");
                        String mfrn = c.getString("MOTHER_FOURTH_NAME");
                        String cd = c.getString("CREATED_DATE");
                        String ld = c.getString("LUDT");
                        String lb = c.getString("LUBY");
                        String si = c.getString("STATE_ID");
                        String fh = c.getString("FAMILY_HEAD");
                        String pc = c.getString("POSTCODE");
                        String cs = c.getString("CITIZEN_STATUS");
                        String ni = c.getString("NATURALIZATION_ID");
                        String ri = c.getString("REGION_ID");
                        String cv = c.getString("CAN_VOTE");
                        String sipi = c.getString("STATE_ID_PROCESS_ID");
                        String ptn = c.getString("PROCESS_TOKEN_NO");
                        String sis = c.getString("STATE_ID_STATUS");
                        String sips = c.getString("STATE_ID_PROCESS_STATUS");
                        String fsi = c.getString("FAMILY_STATE_ID");
                        String pl = c.getString("PICKUP_LOCATION");
                        String el = c.getString("ENROLL_LOCATION");
                        String ed = c.getString("ENROLL_DISTRICT");
                        String sd = c.getString("SERVICE_DISTRICT");
                        String tob = c.getString("TIME_OF_BIRTH");
                        String bcps = c.getString("BIRTH_CERTIFICATE_PROCESS_STATUS");
                        String acps = c.getString("AUTH_CERTIFICATE_PROCESS_STATUS");
                        String ffn = c.getString("FATHER_FIRST_NAME");
                        String fsn = c.getString("FATHER_SECOND_NAME");
                        String ftn = c.getString("FATHER_THIRD_NAME");
                        String ffrn = c.getString("FATHER_FOURTH_NAME");
                        db.execSQL("insert into person (ID  , FIRST_NAME  , SECOND_NAME  , THIRD_NAME  ,FOURTH_NAME  , MARITIAL_STATUS  , LIVING_STATUS ,GENDER  , ADDRESS  , CITY ,REGION   , PHONE_NUMBER  , MOBILE_NUMBER  , EMAIL  ,DATE_OF_BIRTH  , PLACE_OF_BIRTH  , HEIGHT ,WEIGHT  , EYE_COLOR  , HAIR_COLOR ,OCCUPATION  , MOTHER_FIRST_NAME  , MOTHER_SECOND_NAME ,MOTHER_THIRD_NAME  , MOTHER_FOURTH_NAME ,  CREATED_DATE  , LUDT ,LUBY  , STATE_ID  , FAMILY_HEAD , POSTCODE  ,CITIZEN_STATUS  , NATURALIZATION_ID  , REGION_ID ,CAN_VOTE , STATE_ID_PROCESS_ID  , PROCESS_TOKEN_NO ,STATE_ID_STATUS , STATE_ID_PROCESS_STATUS  , FAMILY_STATE_ID  , PICKUP_LOCATION  ,ENROLL_LOCATION  , ENROLL_DISTRICT  , SERVICE_DISTRICT ,TIME_OF_BIRTH  , BIRTH_CERTIFICATE_PROCESS_STATUS  , AUTH_CERTIFICATE_PROCESS_STATUS ,FATHER_FIRST_NAME , FATHER_SECOND_NAME  , FATHER_THIRD_NAME ,FATHER_FOURTH_NAME) values('" + id + "','" + fn + "','" + sn + "','" + tn + "','" + frn + "','" + ms + "','" + ls + "','" + g + "','" +a1 + "','" + cy + "','" + r + "','" + pn + "','" + mn + "','" + e + "','" + dob + "','" + pob + "','" + h + "','" + wgt + "','" + ec + "','" + hc + "','" + o + "','" + mfn + "','" + msn + "','" + mtn + "','" + mfrn + "','" + cd + "','" + ld + "','" + lb + "','" + si + "','" + fh + "','" + pc + "','" + cs + "','" + ni + "','" + ri + "','" + cv + "','" + sipi + "','" + ptn + "','" + sis + "','" + sips + "','" + fsi + "','" + pl + "','" + el + "','" + ed + "','" + sd + "','" + tob + "','" + bcps + "','" + acps + "','" + ffn + "','" + fsn + "','" + ftn + "','" + ffrn + "')");

                        // creating new HashMap
                        // HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        // map.put(TAG_PID, id);
                        // map.put(TAG_NAME, name);
//
                        // adding HashList to ArrayList
                        // productsList.add(map);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            // pDialog.dismiss();
           // finish();
           new vehicledetail().execute();
        }

    }
    class vehicledetail extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            db= openOrCreateDatabase("mydb1", MODE_PRIVATE, null);

            db.execSQL("create table if not exists Vehicle (v_id VARCHAR , l_p VARCHAR , vin VARCHAR , year VARCHAR ,make VARCHAR ,model VARCHAR ,type VARCHAR ,color VARCHAR ,odometer VARCHAR ,title VARCHAR ,taxbase VARCHAR ,purchase_price VARCHAR ,vehicle_wait VARCHAR ,inspection_status VARCHAR ,inspection_expiry VARCHAR ,engine_capacity VARCHAR ,engine_no VARCHAR ,vin_chasis , fuel_type VARCHAR ,security_interest VARCHAR , luby VARCHAR , ludt date ,current_title_doc VARCHAR ,tax_valid_month VARCHAR ,tax_valid_year VARCHAR,tax_num VARCHAR ,tax_expiry_date VARCHAR ,sub_category VARCHAR ,pl VARCHAR , created_date VARCHAR )");



        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products1, "GET", params);
            Runtime.getRuntime().gc();
            // Check your log cat for JSON reponse
        //    Log.d("All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        String vid = c.getString("vehicle_id");
                        String lp = c.getString("License_Plate");
                        String vin = c.getString("VIN");
                        String y = c.getString("Year");
                        String ma = c.getString("Make");
                        String mo = c.getString("Model");
                        String ty = c.getString("Type");
                        String cr = c.getString("Color");
                        String od = c.getString("Odometer");
                        String tit = c.getString("Title");
                        String tb = c.getString("TaxBase");
                        String pp = c.getString("Purchase_Price");
                        String vw = c.getString("vehicle_weight");
                        String is = c.getString("inspection_status");
                        String ie = c.getString("inspection_expiry");
                        String ec = c.getString("engine_capacity");
                        String en = c.getString("engine_number");
                        String vc = c.getString("vin_chasis");
                        String ft = c.getString("fuel_type");
                        String si = c.getString("security_interest");
                        String lb = c.getString("LUBY");
                        String ld = c.getString("LUDT");
                        String ctdt = c.getString("current_title_document_type");
                        String tvum = c.getString("tax_valid_until_month");
                        String tvuy = c.getString("tax_valid_until_year");
                        String tn = c.getString("tax_number");
                        String te = c.getString("tax_expiry_date");
                        String sc = c.getString("sub_category");
                        String pl = c.getString("pl");
                        String cd = c.getString("created_date");
                        db.execSQL("insert into Vehicle(v_id  , l_p  , vin  , year  ,make  ,model  ,type  ,color  ,odometer  ,title  ,taxbase  ,purchase_price  ,vehicle_wait  ,inspection_status  ,inspection_expiry  ,engine_capacity  ,engine_no  ,vin_chasis , fuel_type   ,security_interest  , luby  , ludt  ,current_title_doc  ,tax_valid_month  ,tax_valid_year ,tax_num  ,tax_expiry_date  ,sub_category  ,pl  , created_date ) values('" + vid + "','" + lp + "','" + vin + "','" + y + "','" + ma + "','" + mo + "','" + ty + "','" + cr + "','" + od + "','" + tit + "','" + tb + "','" + pp + "','" + vw + "','" + is + "','" + ie + "','" + ec + "','" + en + "','" + vc + "','" + ft + "','" + si + "','" + lb + "','" + ld + "','" + ctdt + "','" + tvum + "','" + tvuy + "','" + tn + "','" + te + "','" + sc + "','" + pl + "','" + cd + "')");

                        // creating new HashMap
                        // HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        // map.put(TAG_PID, id);
                        // map.put(TAG_NAME, name);
//
                        // adding HashList to ArrayList
                        // productsList.add(map);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
             pDialog.dismiss();
            finish();
            //new CreateNewProduct().execute();
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

