package com.example.admin.cp;


import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Blufour1 on 01-04-2015.
 */
public class NotFound extends Activity {
    LinearLayout ll;
    EditText makeEditNot,modelEditNot,typeEditNot,e1,e2,e3;
    SQLiteDatabase db=null;
    String s;
    Button btnSaveNot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.not_found);

        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("ADD Vehicle");


        show("Create new vehicle detail");
        e1=(EditText)findViewById(R.id.checkSearch);
        e2=(EditText)findViewById(R.id.Search);
        Bundle bundle = this.getIntent().getExtras();
        ll=(LinearLayout)findViewById(R.id.l);
        s = bundle.getString("pn");
if(constants.add.equals("true"))
{
    ll.setVisibility(View.VISIBLE);
    e3=(EditText)findViewById(R.id.editPlate1);

}





        db=openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        Cursor c3;
        c3 = db.rawQuery("select * from CheckPoint", null);
        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e1.setText(pn);
                // a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e2.setText(strDate);
        e1.setClickable(false);
        e1.setFocusable(false);
        e2.setClickable(false);
        e2.setFocusable(false);



        makeEditNot=(EditText)findViewById(R.id.makeEditNot);
        modelEditNot=(EditText)findViewById(R.id.modelEditNot);
        typeEditNot=(EditText)findViewById(R.id.typeEditNot);
        btnSaveNot=(Button)findViewById(R.id.btnSaveNot);

        db=openOrCreateDatabase("mydb1", MODE_PRIVATE, null);

        btnSaveNot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if(constants.add.equals("true")) {
                    s=e3.getText().toString();
                }
                db.execSQL("insert into Checkpoint_Trip (PLATE_NUMBER  , MAKE ,MODEL ,TYPE ) values('" + s + "','" + makeEditNot.getText().toString() + "','" + modelEditNot.getText().toString() + "','" + typeEditNot.getText().toString() + "')");
                constants.ma=makeEditNot.getText().toString();
                constants.mo=modelEditNot.getText().toString();
                Log.d("typeEditNot",""+typeEditNot);
                constants.pn=s;
                Intent i=new Intent(NotFound.this,MainActivity.class);
                startActivity(i);
                constants.rtav="yes";

            }
        });

    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }
}