package com.example.admin.cp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Admin on 01-04-2015.
 */
public class CreatePassengerActivity extends Activity {
    EditText e1,e2,e3,e4,e5,e6;
    SQLiteDatabase db=null;
    String n,rn,d,cpid;
    int id;
    String s;
    private CheckBox ck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createpassenger);
        Bundle bundle = this.getIntent().getExtras();
        //show("create new passenger Detail");
        n = bundle.getString("pid");
        rn = bundle.getString("rn");
        ck   = (CheckBox) findViewById(R.id.checkBox);
        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
      //  getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("ADD PERSON");


        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        e1 = (EditText) findViewById(R.id.textView);
        e2 = (EditText) findViewById(R.id.textView4);
        e3 = (EditText) findViewById(R.id.textView6);
        e4 = (EditText) findViewById(R.id.textView8);
        e5 = (EditText) findViewById(R.id.textView11);
        e6 = (EditText) findViewById(R.id.editText);

      //  db.execSQL("create table if not exists addperson (ID INTEGER PRIMARY KEY AUTOINCREMENT,pnid VARCHAR , LICENSE VARCHAR , NAME VARCHAR , DOB VARCHAR , ADDRESS VARCHAR , GENDER VARCHAR )");




        }
    public void click3 (View v) {


            if (ck.isChecked()) {
                s = "True";
            } else {
                s = "false";
            }



        constants.an.add(e3.getText().toString());
        constants.vn.add(e1.getText().toString());
        Cursor cc = db.rawQuery("select * from CheckPoint", null);
        if (cc.getCount() > 0) {
            cc.moveToFirst();
            do {
                id = cc.getInt(cc.getColumnIndex("ID"));
                cpid = cc.getString(1);

            } while (cc.moveToNext());
        }
       show("data saved");
       // db.execSQL("insert into addperson (pnid , LICENSE , NAME , DOB , ADDRESS , GENDER ) values('" + e1.getText().toString() + "','" + e2.getText().toString() + "','" + e3.getText().toString() + "','" + e4.getText().toString() + "','" + e5.getText().toString() + "','" + e6.getText().toString() + "')");
        db.execSQL("insert into PersonDetails(CHECK_POINT_CROSS_ID ,  PERSON_ID , PERSON_REF_NO  , NAME , DATE_OF_BIRTH ,  ADDRESS ,VERIFICATION_STATUS,IS_DRIVER ) values('" + cpid  + "','" + e1.getText().toString() + "','" + e2.getText().toString() + "','" + e3.getText().toString() + "','" + e4.getText().toString() + "','" + e5.getText().toString() + "','" + "yes" + "','" + s + "')");
        Intent intent = new Intent(CreatePassengerActivity.this, MainActivity.class);

       // intent.putExtra("pid", e1.getText().toString());
       // intent.putExtra("name", e3.getText().toString());
       // intent.putExtra("dob", e4.getText().toString());
        //intent.putExtra("ad", e5.getText().toString());
        //intent.putExtra("g", e6.getText().toString());
       // intent.putExtra("rn", rn);

       // intent.putExtra("cpid", cpid);
        startActivity(intent);
    }
    public void click4 (View v) {
        show("entry cancelled");
        Intent intent = new Intent(CreatePassengerActivity.this, MainActivity.class);
        startActivity(intent);
    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

}



