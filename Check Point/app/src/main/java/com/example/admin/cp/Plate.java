package com.example.admin.cp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class Plate extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    SharedPreferences mPrefs;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    EditText editPlate,e1,e2;
    ArrayList<String> a= new ArrayList();
    ArrayList<String> b= new ArrayList();
    ImageView go;
    String ck="true";
    SQLiteDatabase db = null;
    String pn;
    Cursor c,c1=null,c3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.plate_number);
        constants.rtav=null;
        constants. cid=1;
        constants. cpkd=null;
        constants. rtar="";
        constants. pn=null;
        constants. ma=null;
        constants.mo=null;
        constants.  p="true";
        constants. vs=null;
        constants.an= new ArrayList();
        constants. an1= new ArrayList();
        constants. vn= new ArrayList();
        constants. vn1= new ArrayList();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean welcomeScreenShown = mPrefs.getBoolean("j", false);

        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        // getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("VEHICLE SEARCH");
        editPlate=(EditText)findViewById(R.id.editPlate);
        e1=(EditText)findViewById(R.id.checkSearch);
        e2=(EditText)findViewById(R.id.Search);
        mTitle = mDrawerTitle = getTitle();
        mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        //      mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mPlanetTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        getActionBar().setIcon(android.R.color.transparent);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.hamburger,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("MENU");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("MENU");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        // new LoadAllProducts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        go=(ImageView)findViewById(R.id.go);

        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
   //     db.execSQL("create table if not exists person (ID INTEGER , FIRST_NAME VARCHAR , SECOND_NAME VARCHAR , THIRD_NAME VARCHAR ,FOURTH_NAME VARCHAR , MARITIAL_STATUS VARCHAR , LIVING_STATUS VARCHAR,GENDER VARCHAR , ADDRESS VARCHAR , CITY VARCHAR,REGION VARCHAR , PHONE_NUMBER VARCHAR , MOBILE_NUMBER VARCHAR , EMAIL VARCHAR ,DATE_OF_BIRTH VARCHAR , PLACE_OF_BIRTH VARCHAR , HEIGHT VARCHAR,WEIGHT VARCHAR , EYE_COLOR VARCHAR , HAIR_COLOR VARCHAR,OCCUPATION VARCHAR , MOTHER_FIRST_NAME VARCHAR , MOTHER_SECOND_NAME VARCHAR,MOTHER_THIRD_NAME VARCHAR , MOTHER_FOURTH_NAME VARCHAR ,  CREATED_DATE VARCHAR , LUDT VARCHAR,LUBY VARCHAR , STATE_ID VARCHAR , FAMILY_HEAD VARCHAR  , POSTCODE VARCHAR ,CITIZEN_STATUS VARCHAR , NATURALIZATION_ID VARCHAR , REGION_ID VARCHAR,CAN_VOTE VARCHAR , STATE_ID_PROCESS_ID VARCHAR , PROCESS_TOKEN_NO VARCHAR,STATE_ID_STATUS VARCHAR  , STATE_ID_PROCESS_STATUS VARCHAR , FAMILY_STATE_ID VARCHAR , PICKUP_LOCATION VARCHAR ,ENROLL_LOCATION VARCHAR , ENROLL_DISTRICT VARCHAR , SERVICE_DISTRICT VARCHAR,TIME_OF_BIRTH VARCHAR , BIRTH_CERTIFICATE_PROCESS_STATUS VARCHAR , AUTH_CERTIFICATE_PROCESS_STATUS VARCHAR,FATHER_FIRST_NAME VARCHAR , FATHER_SECOND_NAME VARCHAR , FATHER_THIRD_NAME VARCHAR,FATHER_FOURTH_NAME VARCHAR   )");



        constants.cid= constants.cid+1;
        // db.execSQL("create table if not exists Trip (ID INTEGER PRIMARY KEY AUTOINCREMENT,PLATE_NUMBER VARCHAR , MAKE VARCHAR,MODEL VARCHAR,TYPE VARCHAR )");
        //  db.execSQL("insert into Trip (PLATE_NUMBER , MAKE , MODEL , TYPE) values('" + "TN3030" + "','" + "Lenovo" + "','"+"Honda"+"','"+ "Lamburgini"+"')");

        if (!welcomeScreenShown) {
            db.execSQL("create table if not exists person (ID INTEGER , FIRST_NAME VARCHAR , SECOND_NAME VARCHAR , THIRD_NAME VARCHAR ,FOURTH_NAME VARCHAR , MARITIAL_STATUS VARCHAR , LIVING_STATUS VARCHAR,GENDER VARCHAR , ADDRESS VARCHAR , CITY VARCHAR,REGION VARCHAR , PHONE_NUMBER VARCHAR , MOBILE_NUMBER VARCHAR , EMAIL VARCHAR ,DATE_OF_BIRTH VARCHAR , PLACE_OF_BIRTH VARCHAR , HEIGHT VARCHAR,WEIGHT VARCHAR , EYE_COLOR VARCHAR , HAIR_COLOR VARCHAR,OCCUPATION VARCHAR , MOTHER_FIRST_NAME VARCHAR , MOTHER_SECOND_NAME VARCHAR,MOTHER_THIRD_NAME VARCHAR , MOTHER_FOURTH_NAME VARCHAR ,  CREATED_DATE VARCHAR , LUDT VARCHAR,LUBY VARCHAR , STATE_ID VARCHAR , FAMILY_HEAD VARCHAR  , POSTCODE VARCHAR ,CITIZEN_STATUS VARCHAR , NATURALIZATION_ID VARCHAR , REGION_ID VARCHAR,CAN_VOTE VARCHAR , STATE_ID_PROCESS_ID VARCHAR , PROCESS_TOKEN_NO VARCHAR,STATE_ID_STATUS VARCHAR  , STATE_ID_PROCESS_STATUS VARCHAR , FAMILY_STATE_ID VARCHAR , PICKUP_LOCATION VARCHAR ,ENROLL_LOCATION VARCHAR , ENROLL_DISTRICT VARCHAR , SERVICE_DISTRICT VARCHAR,TIME_OF_BIRTH VARCHAR , BIRTH_CERTIFICATE_PROCESS_STATUS VARCHAR , AUTH_CERTIFICATE_PROCESS_STATUS VARCHAR,FATHER_FIRST_NAME VARCHAR , FATHER_SECOND_NAME VARCHAR , FATHER_THIRD_NAME VARCHAR,FATHER_FOURTH_NAME VARCHAR   )");
            db.execSQL("create table if not exists Vehicle (v_id VARCHAR , l_p VARCHAR , vin VARCHAR , year VARCHAR ,make VARCHAR ,model VARCHAR ,type VARCHAR ,color VARCHAR ,odometer VARCHAR ,title VARCHAR ,taxbase VARCHAR ,purchase_price VARCHAR ,vehicle_wait VARCHAR ,inspection_status VARCHAR ,inspection_expiry VARCHAR ,engine_capacity VARCHAR ,engine_no VARCHAR ,vin_chasis , fuel_type VARCHAR ,security_interest VARCHAR , luby VARCHAR , ludt date ,current_title_doc VARCHAR ,tax_valid_month VARCHAR ,tax_valid_year VARCHAR,tax_num VARCHAR ,tax_expiry_date VARCHAR ,sub_category VARCHAR ,pl VARCHAR , created_date VARCHAR )");
            db.execSQL("insert into person (ID  , FIRST_NAME  , SECOND_NAME  , THIRD_NAME  ,FOURTH_NAME  , MARITIAL_STATUS  , LIVING_STATUS ,GENDER  , ADDRESS  , CITY ,REGION   , PHONE_NUMBER  , MOBILE_NUMBER  , EMAIL  ,DATE_OF_BIRTH  , PLACE_OF_BIRTH  , HEIGHT ,WEIGHT  , EYE_COLOR  , HAIR_COLOR ,OCCUPATION  , MOTHER_FIRST_NAME  , MOTHER_SECOND_NAME ,MOTHER_THIRD_NAME  , MOTHER_FOURTH_NAME ,  CREATED_DATE  , LUDT ,LUBY  , STATE_ID  , FAMILY_HEAD , POSTCODE  ,CITIZEN_STATUS  , NATURALIZATION_ID  , REGION_ID ,CAN_VOTE , STATE_ID_PROCESS_ID  , PROCESS_TOKEN_NO ,STATE_ID_STATUS , STATE_ID_PROCESS_STATUS  , FAMILY_STATE_ID  , PICKUP_LOCATION  ,ENROLL_LOCATION  , ENROLL_DISTRICT  , SERVICE_DISTRICT ,TIME_OF_BIRTH  , BIRTH_CERTIFICATE_PROCESS_STATUS  , AUTH_CERTIFICATE_PROCESS_STATUS ,FATHER_FIRST_NAME , FATHER_SECOND_NAME  , FATHER_THIRD_NAME ,FATHER_FOURTH_NAME) values('" + "333" + "','" + "ABDUL" + "','" + "farooq" + "','" + "farooq" + "','" + "farooq" + "','" + "married" + "','" + "alive" + "','" + "male" + "','" +"HANTIWADAG" + "','" + "sydney" + "','" + "ddd" + "','" + "5454534" + "','" + "444" + "','" + "j@gmail.vom" + "','" + "1977-12-01" + "','" + "salem" + "','" + "170" + "','" + "60" + "','" + "black" + "','" + "blue" + "','" + "weaving" + "','" + "lakshmi" + "','" + "lakshmi" + "','" + "lakshmi" + "','" + "lakshmi" + "','" + "2013-12-21 13:56:07" + "','" + "2014-01-29 02:30:01" + "','" + "officer" + "','" + "A51-000-0001" + "','" + "n" + "','" + "pc" + "','" + "cs" + "','" + "ni" + "','" + "ri" + "','" + "cv" + "','" + "sipi" + "','" + "ptn" + "','" + "sis" + "','" + "sips" + "','" + "fsi" + "','" + "pl" + "','" + "el" + "','" + "ed" + "','" + "sd" + "','" + "tob" + "','" + "bcps" + "','" + "acps" + "','" + "ffn" + "','" + "fsn" + "','" + "ftn" + "','" + "ffrn" + "')");
            db.execSQL("insert into Vehicle(v_id  , l_p  , vin  , year  ,make  ,model  ,type  ,color  ,odometer  ,title  ,taxbase  ,purchase_price  ,vehicle_wait  ,inspection_status  ,inspection_expiry  ,engine_capacity  ,engine_no  ,vin_chasis , fuel_type   ,security_interest  , luby  , ludt  ,current_title_doc  ,tax_valid_month  ,tax_valid_year ,tax_num  ,tax_expiry_date  ,sub_category  ,pl  , created_date ) values('" + "BOS-2878-30" + "','" + "10136" + "','" + "vin" + "','" + "1993" + "','" + "Toyota" + "','" + "MARK 2" + "','" + "seden" + "','" + "black" + "','" + "od" + "','" + "tit" + "','" + "tb" + "','" + "pp" + "','" + "vw" + "','" + "is" + "','" + "ie" + "','" + "ec" + "','" + "en" + "','" + "vc" + "','" + "ft" + "','" + "si" + "','" + "lb" + "','" + "ld" + "','" + "ctdt" + "','" + "tvum" + "','" + "tvuy" + "','" + "tn" + "','" + "te" + "','" + "sc" + "','" + "pl" + "','" + "cd" + "')");


            db.execSQL("create table if not exists Checkpoint_Trip (ID INTEGER PRIMARY KEY AUTOINCREMENT,CHECK_POINT_TRIP_ID VARCHAR,PLATE_NUMBER VARCHAR ,RTA_VERIFICATION VARCHAR,RTA_REFERENCE_NUMBER VARCHAR, MAKE VARCHAR,MODEL VARCHAR,TYPE VARCHAR,SOURCE VARCHAR,DESTINATION VARCHAR,CAR_PARKED VARCHAR )");

            db.execSQL("insert into Checkpoint_Trip (CHECK_POINT_TRIP_ID ,PLATE_NUMBER  ,RTA_VERIFICATION ,RTA_REFERENCE_NUMBER , MAKE ,MODEL ,TYPE ,SOURCE ,DESTINATION ,CAR_PARKED   ) values('" + "xyz0001"+ "','" + "TN3010"+ "','" + "yes"+ "','" +"ref0001"+ "','" +"benze"+ "','" +"b0001"+ "','" +"normal"+ "','" +"chennai" + "','" + "coimbatore" + "','" + "no" + "')");

            db.execSQL("insert into Checkpoint_Trip (CHECK_POINT_TRIP_ID ,PLATE_NUMBER  ,RTA_VERIFICATION ,RTA_REFERENCE_NUMBER , MAKE ,MODEL ,TYPE ,SOURCE ,DESTINATION ,CAR_PARKED   ) values('" + "xyz0003"+ "','" + "TN3050"+ "','" + "yes"+ "','" +"ref0003"+ "','" +"benze"+ "','" +"b0003"+ "','" +"normal"+ "','" +"chennai" + "','" + "coimbatore" + "','" + "no" + "')");

            db.execSQL("insert into Checkpoint_Trip (CHECK_POINT_TRIP_ID ,PLATE_NUMBER  ,RTA_VERIFICATION ,RTA_REFERENCE_NUMBER , MAKE ,MODEL ,TYPE ,SOURCE ,DESTINATION ,CAR_PARKED   ) values('" + "xyz0002"+ "','" + "TN3030"+ "','" + "yes"+ "','" +"ref0002"+ "','" +"benze"+ "','" +"b0002"+ "','" +"normal"+ "','" +"chennai" + "','" + "coimbatore" + "','" + "no" + "')");


            db.execSQL("create table if not exists RTA (ID INTEGER PRIMARY KEY AUTOINCREMENT,PLATE_NUMBER VARCHAR , MAKE VARCHAR,MODEL VARCHAR,TYPE VARCHAR , DL VARCHAR , RNO VARCHAR )");

            db.execSQL("insert into RTA (PLATE_NUMBER , MAKE , MODEL , TYPE , DL , RNO) values('" + "TN3020" + "','" + "Dell" + "','"+"Bmw"+"','"+ "Lotus"+"','"+ "dl0005"+"','"+ "rf0005"+"')");

            db.execSQL("insert into RTA (PLATE_NUMBER , MAKE , MODEL , TYPE , DL , RNO) values('" + "TN3040" + "','" + "Bell" + "','"+"Bmw"+"','"+ "Lotus"+"','"+ "dl0006"+"','"+ "rf0005"+"')");
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putBoolean("j", true);
            editor.commit();
        }
        c3 = db.rawQuery("select * from CheckPoint", null);

        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e1.setText(pn);
                // a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e2.setText(strDate);
        e1.setClickable(false);
        e1.setFocusable(false);
        e2.setClickable(false);
        e2.setFocusable(false);


        // c = db.rawQuery("select * from VehicleDetails WHERE  driver_license_number='"+editPlate.getText().toString()+"'" , null);

        //  c1=db.rawQuery("select * from RTA ", null);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editPlate.getText().toString().equals(""))

                {
                    show("enter value");
                } else {
                    c = db.rawQuery("select * from Vehicle WHERE  l_p='"+editPlate.getText().toString()+"'" , null);
                   // c = db.rawQuery("select * from Vehicle WHERE  v_id='"+editPlate.getText().toString()+"'" , null);
                    System.out.println("in else");
                    if (c.getCount() > 0) {
                        System.out.println("in if");
                        c.moveToFirst();
                        do {
                            pn = c.getString(1);
                            System.out.println("in do");
                            a.add(pn);
                        } while (c.moveToNext());

                        Intent i1 = new Intent(Plate.this, MatchFound.class);
                        i1.putExtra("pn", pn);
                        constants.pn = editPlate.getText().toString();
                        startActivity(i1);

                    }
                    else
                    {
                        constants.add="true";

                        Intent i2 = new Intent(Plate.this, NotFound.class);
                        i2.putExtra("pn", editPlate.getText().toString());
                        constants.pn = editPlate.getText().toString();
                        startActivity(i2);

                    }
             /*   if (c.getCount() > 0) {
                    c.moveToFirst();
                    do {
                        String pn = c.getString(2);

                        a.add(pn);
                    } while (c.moveToNext());

                    for (int i = 0; i < c.getCount(); i++) {

                        if (editPlate.getText().toString().equalsIgnoreCase(a.get(i)))

                        {
                            ck = "false";
                            constants.pn = a.get(i);
                            Intent i1 = new Intent(Plate.this, MatchFound.class);
                            i1.putExtra("pn", a.get(i));
                            startActivity(i1);
                            finish();

                            break;
                        }
                    }
                } else {

                }
                if (c1.getCount() > 0) {
                    c1.moveToFirst();
                    do {
                        // show("ada");
                        System.out.println("in do");
                        String pn = c1.getString(1);
                        //   constants.pn = editPlate.getText().toString();
                        b.add(pn);
                    } while (c1.moveToNext());
                    System.out.println("out do");
                    for (int i12 = 0; i12 < c1.getCount(); i12++) {
                        System.out.println("in for");
                        if (editPlate.getText().toString().equalsIgnoreCase(b.get(i12)))

                        {
                            show(b.get(i12));
                            System.out.println("1" + b.get(i12));
                            System.out.println("2" + editPlate.getText().toString());
                            ck = "false";
                            constants.pn = b.get(i12);
                            Intent i1 = new Intent(Plate.this, MatchFound.class);
                            i1.putExtra("pn", b.get(i12));
                            startActivity(i1);
                            finish();

                            break;
                        }
                    }
                }
                else {

                }*/
                }
           /*     if (ck.equals("true")) {

                    System.out.println("in else");
                    Intent i2 = new Intent(Plate.this, NotFound.class);
                    i2.putExtra("pn", editPlate.getText().toString());
                    constants.pn = editPlate.getText().toString();
                    startActivity(i2);
                    finish();
            }*/
            }
        });

    }

    public void click (View v) {
        constants.add="true";

        Intent i2 = new Intent(Plate.this, NotFound.class);
        i2.putExtra("pn", editPlate.getText().toString());
        constants.pn = editPlate.getText().toString();
        startActivity(i2);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_settings:
                // create intent to perform web search for this planet

                // catch event that there's no activity to handle intent

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        switch (position) {
            case 0:

                Intent intent = new Intent(Plate.this, Plate.class);

                startActivity(intent);
                mDrawerLayout.closeDrawers();


                break;
            case 1:
                Intent intent2 = new Intent(Plate.this, syncActivity.class);

                startActivity(intent2);
                mDrawerLayout.closeDrawers();
                break;
            case 2:
                Intent intent1 = new Intent(Plate.this, HomeActivity.class);

                startActivity(intent1);
                mDrawerLayout.closeDrawers();
                break;
            case 3:

                break;
            case 4:

                break;
            case 5:

                break;

            default:
                break;
        }

    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle("MENU");
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Fragment that appears in the "content_frame", shows a planet
     */

    /* Called whenever we call invalidateOptionsMenu() */
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }
}