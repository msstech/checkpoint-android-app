package com.example.admin.cp;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

/**
 * Created by Blufour1 on 01-04-2015.
 */
public class Preview extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.preview);

        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("VEHILE DETAILS");
    }
}