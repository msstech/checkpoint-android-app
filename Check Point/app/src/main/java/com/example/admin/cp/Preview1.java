package com.example.admin.cp;


        import android.app.Activity;
        import android.app.Notification;
        import android.app.NotificationManager;
        import android.content.Context;
        import android.content.Intent;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Color;
        import android.graphics.Point;
        import android.graphics.Typeface;
        import android.graphics.drawable.ColorDrawable;
        import android.os.Bundle;
        import android.os.Environment;
        import android.support.v4.app.NotificationCompat;
        import android.util.Base64;
        import android.util.Log;
        import android.view.Display;
        import android.view.View;
        import android.view.WindowManager;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.zxing.BarcodeFormat;
        import com.google.zxing.WriterException;

        import java.io.BufferedOutputStream;
        import java.io.ByteArrayInputStream;
        import java.io.File;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;

/**
 * Created by Admin on 30-03-2015.
 */
public class Preview1 extends Activity {
    SQLiteDatabase db=null;
    String vn,vtr,Sr,Dt;
int id;
    EditText Veh_Tno,Veh_no,PassengerCount,PassengerAdult,PassengerChild,PassengerInfat,RoundTrip,Source,Destination,DriverIdentity;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_main);
        Bundle bundle = this.getIntent().getExtras();
      //  id = bundle.getString("id");
        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setTitle("PREVIEW");
        Veh_no = (EditText) findViewById(R.id.vehicle_number);

        Veh_Tno = (EditText) findViewById(R.id.vehicle_track_number);



        Source = (EditText) findViewById(R.id.source);

        Destination = (EditText) findViewById(R.id.destination);



        System.out.println("prev"+constants.pn);
        Cursor c= db.rawQuery("select * from Checkpoint_Trip WHERE PLATE_NUMBER='"+constants.pn+"'", null);
        if (c.getCount() > 0) {


            //  tv.setText("");
            //move cursor to first position
            c.moveToFirst();
            //fetch all data one by one
            do {
                 id = c.getInt(c.getColumnIndex("ID"));
                 vtr = c.getString(1);
                 vn = c.getString(2);

                 Sr = c.getString(8);
                 Dt = c.getString(9);


            } while (c.moveToNext());
        }
        else
        {
           show("no match"+constants.pn) ;
        }
                Veh_no.setText(vn);
                Veh_Tno.setText(vtr);


                Source.setText(Sr);
                Destination.setText(Dt);

        Veh_no.setClickable(false);
        Veh_no.setFocusable(false);
        Veh_Tno.setClickable(false);
        Veh_Tno.setFocusable(false);
        Source.setClickable(false);
        Source.setFocusable(false);
        Destination.setClickable(false);
        Destination.setFocusable(false);
           // } while (c.moveToNext());


        //}
    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }
    public void onClick1(View v)
    {
        Intent i2 = new Intent(Preview1.this, Plate.class);
        startActivity(i2);

    }









public void Print(View v)throws IOException

{

    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancel(100000);
    notificationManager.cancelAll();
    //Find screen size
    WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
    Display display = manager.getDefaultDisplay();
    Point point = new Point();
    display.getSize(point);
    int width = point.x;
    int height = point.y;
    int smallerDimension = width < height ? width : height;
    smallerDimension = smallerDimension * 3/4;

    //Encode with a QR Code image
    QRCodeEncoder qrCodeEncoder = new QRCodeEncoder("veh0046dhdh",
            null,
            Contents.Type.TEXT,
            BarcodeFormat.QR_CODE.toString(),
            smallerDimension);
    try {
        Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
        ImageView myImage = (ImageView) findViewById(R.id.imageView);
        myImage.setImageBitmap(bitmap);
        storeImage(bitmap, "jai");
    }catch (WriterException e) {
        e.printStackTrace();
    }


    EAN13CodeBuilder bb = new EAN13CodeBuilder("123456789423");

    TextView t = (TextView)findViewById(R.id.barcode);
    Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/EanP72Tt Normal.Ttf");
    t.setTypeface(font);
    t.setText(bb.getCode());
    InputStream stream = new ByteArrayInputStream(Base64.decode(bb.getCode().getBytes(), Base64.DEFAULT));

    Bitmap bitmap1 = StringToBitMap(bb.getCode());

    StringToBitMap(bb.getCode());
    //DB_PATH = QRMConstants.getDatabaseStoragePath(getApplicationContext());
    // Path to the just created empty db
    //String outFileName = DB_PATH + "image";
    // Open the empty db as the output stream
   // OutputStream myOutput = new FileOutputStream(outFileName);
    // transfer bytes from the inputfile to the outputfile
   // byte[] buffer = new byte[1024];
   // int length;
   // while ((length = stream.read(buffer)) > 0) {
    //    myOutput.write(buffer, 0, length);

    // Close the streams
    //myOutput.flush();
    //myOutput.close();
    //stream.close();








}
    private boolean storeImage(Bitmap imageData, String filename) {
        //get path to external storage (SD card)


        String iconsStoragePath = Environment.getExternalStorageDirectory() + "/myAppDir/myImages/";
        File sdIconStorageDir = new File(iconsStoragePath);

        //create storage directories, if they don't exist
        sdIconStorageDir.mkdirs();

        try {
            String filePath = sdIconStorageDir.toString() + filename;
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.PNG, 100, bos);

            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }




}
