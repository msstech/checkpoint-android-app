package com.example.admin.cp;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.os.Bundle;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Admin on 01-04-2015.
 */
public class DispalyActivity   extends Activity {
    String pind,pid,fname,dob,ad,g,rn,d,cpid,n;
    int id;
    EditText e1,e2,e3,e4,e5,e6,e7,e8,e9;
    private NotificationManager mNotificationManager;
    private int notificationID = 100;
    String s;
    private int numMessages = 1;
    NotificationCompat.Builder  mBuilder;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    ListView lv1;
    private CheckBox ck;
    private ActionBarDrawerToggle mDrawerToggle;
    SQLiteDatabase db=null;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        ck   = (CheckBox) findViewById(R.id.checkBox);
        db = openOrCreateDatabase("mydb1", MODE_PRIVATE, null);
        e5=(EditText)findViewById(R.id.checkSearch);
        e6=(EditText)findViewById(R.id.Search);
        e7=(EditText)findViewById(R.id.makeSearch);
        e8=(EditText)findViewById(R.id.modelSearch);
        e9=(EditText)findViewById(R.id.plateSearch);
        e7.setText(constants.pn);
        e8.setText(constants.ma);
        e9.setText(constants.mo);
        Cursor c3;
        c3 = db.rawQuery("select * from CheckPoint", null);

        if (c3.getCount() > 0) {
            c3.moveToFirst();
            do {
                String pn = c3.getString(1);
                e5.setText(pn);
                // a.add(pn);
            } while (c3.moveToNext());

        }
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String strDate = sdf.format(ca.getTime());
        e6.setText(strDate);
        e5.setClickable(false);
        e5.setFocusable(false);
        e6.setClickable(false);
        e6.setFocusable(false);
        e7.setClickable(false);
        e7.setFocusable(false);
        e8.setClickable(false);
        e8.setFocusable(false);
        e9.setClickable(false);
        e9.setFocusable(false);

        lv1 = (ListView) findViewById(R.id.country_list);





        getActionBar().setIcon(android.R.color.transparent);
        ColorDrawable colorDrawable = new ColorDrawable(
                Color.parseColor("#356AA0"));
        getActionBar().setBackgroundDrawable(colorDrawable);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setTitle("PERSON DETAIL");
          mBuilder =
                new NotificationCompat.Builder(this);







        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        e3 = (EditText) findViewById(R.id.e3);
        e4 = (EditText) findViewById(R.id.e4);
        Bundle bundle = this.getIntent().getExtras();
        pind = bundle.getString("id");
        n = bundle.getString("pid");
        fname = bundle.getString("name");
        dob = bundle.getString("dob");
        ad = bundle.getString("ad");
        g = bundle.getString("g");
        rn = bundle.getString("rn");

        cpid = bundle.getString("cpid");
      //  pid = bundle.getString("pn");
      //  dl = bundle.getString("dl");
      //  pind = bundle.getString("id");
        e1.setText(fname);
        e2.setText(dob);
        e3.setText(ad);
        e4.setText(g);
        e1.setFocusable(false);
        e1.setClickable(false);
        e2.setFocusable(false);
        e2.setClickable(false);
        e3.setFocusable(false);
        e3.setClickable(false);
        e4.setFocusable(false);
        e4.setClickable(false);
        mBuilder.setContentTitle("Person Verification");

        mBuilder.setContentText(pid+"  Verified");
        mBuilder.setTicker("Status Alert!");
        mBuilder.setSmallIcon(R.drawable.noti);

      /* Increase notification number every time a new notification arrives */
        mBuilder.setNumber(++numMessages);



        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);





        }
    public void click (View v) {
        if (ck.isChecked()) {
            s = "True";
        } else {
            s = "false";
        }
        constants.vs="yes";


        db.execSQL("update  PersonDetails set VERIFICATION_STATUS='" +constants.vs  + "', IS_DRIVER='" + s + "' where PERSON_ID = '" + n + "'");
        Intent intent = new Intent(DispalyActivity.this,MainActivity.class);

        startActivity(intent);
      //  mNotificationManager.notify(Integer.parseInt(pind), mBuilder.build());




    }
    public void click1 (View v) {
constants.vs="no";
        db.execSQL("update  PersonDetails set VERIFICATION_STATUS='" +constants.vs  + "', IS_DRIVER='" + d + "' where PERSON_ID = '" + n + "'");
        //db.execSQL("insert into PersonDetails (CHECK_POINT_CROSS_ID  , PERSON_ID  , PERSON_REF_NO  , NAME  , DATE_OF_BIRTH , ADDRESS  , VERIFICATION_STATUS , IS_DRIVER  ) values('" + cpid + "','" + n + "','" + rn + "','" + fname + "','" + dob + "','" + ad + "','" +constants.vs  + "','" + d + "' )");
        show("Details not match");
        Intent intent = new Intent(DispalyActivity.this,Plate.class);

        startActivity(intent);


    }
    public void show(String str)
    {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }




    }


